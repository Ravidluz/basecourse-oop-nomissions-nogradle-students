package AIF.Module;

import AIF.Entities.Coordinates;

public class SensorModule extends Module {
    public SensorModule() {
        super(600);
    }

    @Override
    public void activate(Coordinates destination) {
        System.out.println("Sensor activated on target " + destination.toString());
    }
}
