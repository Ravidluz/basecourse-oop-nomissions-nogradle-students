package AIF.Module;

import AIF.Entities.Coordinates;

public class AttackModule extends Module {
    public AttackModule() {
        super(1500);
    }

    @Override
    public void activate(Coordinates destination) {
        System.out.println("Weapon activated on destination " + destination.toString());
        this.setDeployable(false);
    }
}
