package AIF.Module;

import AIF.Entities.Coordinates;

public abstract class Module {
    private int activationRange;
    private boolean deployable = true;

    public Module(int activationRange) {
        this.setActivationRange(activationRange);
    }

    public abstract void activate(Coordinates destination);

    public int getActivationRange() {
        return activationRange;
    }

    public void setActivationRange(int activationRange) {
        this.activationRange = activationRange;
    }

    public boolean isDeployable() {
        return deployable;
    }

    public void setDeployable(boolean deployable) {
        this.deployable = deployable;
    }
}
