package AIF.Module;

import AIF.Entities.Coordinates;

public class FilmModule extends Module {
    public FilmModule() {
        super(30);
    }

    @Override
    public void activate(Coordinates destination) {
        System.out.println("Film module activated on target " + destination);
    }
}
