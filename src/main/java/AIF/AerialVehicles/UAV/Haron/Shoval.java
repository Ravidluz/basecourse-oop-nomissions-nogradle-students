package AIF.AerialVehicles.UAV.Haron;

import AIF.Entities.Coordinates;
import AIF.Module.AttackModule;
import AIF.Module.FilmModule;
import AIF.Module.SensorModule;

public class Shoval extends Haron {
    public Shoval(Coordinates location) {
        super(location, 3);
        this.addCompatibleModule(AttackModule.class);
        this.addCompatibleModule(FilmModule.class);
        this.addCompatibleModule(SensorModule.class);
    }
}

