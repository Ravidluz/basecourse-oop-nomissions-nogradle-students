package AIF.AerialVehicles.UAV.Haron;

import AIF.Entities.Coordinates;
import AIF.Module.AttackModule;
import AIF.Module.FilmModule;

public class Eitan extends Haron {
    public Eitan(Coordinates location) {
        super(location, 4);
        this.addCompatibleModule(AttackModule.class);
        this.addCompatibleModule(FilmModule.class);
    }

}
