package AIF.AerialVehicles.UAV.Haron;

import AIF.AerialVehicles.UAV.UAV;
import AIF.Entities.Coordinates;

public abstract class Haron extends UAV {
    public Haron(Coordinates location, int modulesStationsAmount) {
        super(location, 15000, modulesStationsAmount);
    }
}
