package AIF.AerialVehicles.UAV;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Entities.Coordinates;

public abstract class UAV extends AerialVehicle {

    final int CIRCULATION_SPEED = 150;

    public UAV(Coordinates currentLocation, int maintenanceDistance, int moduleStationsAmount) {
        super(currentLocation, maintenanceDistance, moduleStationsAmount);
    }

    public void hoverOverLocation(double hours) throws CannotPerformOnGroundException {
        if (this.isOnGround()) {
            System.out.println("Cannot hover while on ground");
            throw new CannotPerformOnGroundException();
        } else {
            System.out.println("Hovering over " + this.getCurrentLocation());
            this.setDistanceSinceLastRepair(this.getDistanceSinceLastRepair() + this.CIRCULATION_SPEED * hours);
        }
    }
}
