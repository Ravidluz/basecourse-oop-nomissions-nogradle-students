package AIF.AerialVehicles.UAV.Hermes;

import AIF.Entities.Coordinates;
import AIF.Module.AttackModule;
import AIF.Module.FilmModule;
import AIF.Module.SensorModule;

public class Kochav extends Hermes {
    public Kochav(Coordinates location) {
        super(location, 5);
        this.addCompatibleModule(AttackModule.class);
        this.addCompatibleModule(FilmModule.class);
        this.addCompatibleModule(SensorModule.class);
    }
}
