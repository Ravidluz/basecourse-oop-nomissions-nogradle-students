package AIF.AerialVehicles.UAV.Hermes;

import AIF.Entities.Coordinates;
import AIF.Module.AttackModule;
import AIF.Module.SensorModule;

public class Zik extends Hermes {
    public Zik(Coordinates location) {
        super(location, 1);
        this.addCompatibleModule(AttackModule.class);
        this.addCompatibleModule(SensorModule.class);
    }
}