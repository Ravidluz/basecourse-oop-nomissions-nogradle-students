package AIF.AerialVehicles.UAV.Hermes;

import AIF.AerialVehicles.UAV.UAV;
import AIF.Entities.Coordinates;

public abstract class Hermes extends UAV {
    public Hermes(Coordinates location, int moduleStationsAmount) {
        super(location, 10000, moduleStationsAmount);
    }
}
