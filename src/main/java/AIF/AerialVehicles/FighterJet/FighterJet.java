package AIF.AerialVehicles.FighterJet;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;
import AIF.Module.AttackModule;

public abstract class FighterJet extends AerialVehicle {
    public FighterJet(Coordinates location, int moduleStationsAmount) {
        super(location, 25000, moduleStationsAmount);
        this.addCompatibleModule(AttackModule.class);
    }
}
