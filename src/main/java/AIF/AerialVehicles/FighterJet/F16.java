package AIF.AerialVehicles.FighterJet;


import AIF.Entities.Coordinates;
import AIF.Module.SensorModule;

public class F16 extends FighterJet {
    public F16(Coordinates coordinates) {
        super(coordinates, 7);
        this.addCompatibleModule(SensorModule.class);
    }
}
