package AIF.AerialVehicles.FighterJet;


import AIF.Entities.Coordinates;
import AIF.Module.FilmModule;

public class F15 extends FighterJet {
    public F15 (Coordinates location) {
        super(location, 10);
        this.addCompatibleModule(FilmModule.class);
    }
}
