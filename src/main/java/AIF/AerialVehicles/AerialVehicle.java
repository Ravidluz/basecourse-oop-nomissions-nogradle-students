package AIF.AerialVehicles;


import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;
import AIF.Module.Module;

import java.util.ArrayList;
import java.util.List;

public abstract class AerialVehicle {
    private boolean onGround;
    private boolean readyForFlight;
    private Coordinates currentLocation;
    private int maintenanceDistance;
    private double distanceSinceLastRepair;
    private final List<Module> moduleList;
    private final List<Class> compatibleModuleList;
    private int moduleStationsAmount;

    public AerialVehicle(Coordinates currentLocation, int maintenanceDistance, int moduleStationsAmount) {
        this.onGround = true;
        this.readyForFlight = true;
        this.setDistanceSinceLastRepair(0);
        this.setModuleStationsAmount(moduleStationsAmount);
        this.moduleList = new ArrayList<>();
        this.compatibleModuleList = new ArrayList<>();
        this.setCurrentLocation(currentLocation);
        this.setMaintenanceDistance(maintenanceDistance);
    }

    public void takeOff() throws NeedMaintenanceException, CannotPerformInMidAirException {
        if (!this.isReadyForFlight()) {
            System.out.println("The aircraft is not ready to fly");
            throw new NeedMaintenanceException();
        } else if (!this.isOnGround()) {
            System.out.println("The aircraft is already in air");
            throw new CannotPerformInMidAirException();
        } else {
            System.out.println("Taking off");
            this.setOnGround(false);
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformOnGroundException {
        if (!this.isOnGround()) {
            System.out.println("Flying to: " + destination.toString());
            this.setDistanceSinceLastRepair(this.getDistanceSinceLastRepair() + currentLocation.distance(destination));
            this.setCurrentLocation(destination);
        } else {
            System.out.println("Cannot fly to anywhere while the aircraft is on ground");
            throw new CannotPerformOnGroundException();
        }
    }

    public void land() throws CannotPerformOnGroundException {
        if (this.isOnGround()) {
            System.out.println("Cannot land while already on ground");
            throw new CannotPerformOnGroundException();
        } else {
            System.out.println("Landing");
            this.setOnGround(true);
        }
    }

    public boolean needMaintenance() {
        return this.getDistanceSinceLastRepair() >= this.getMaintenanceDistance();
    }

    public void performMaintenance() throws CannotPerformInMidAirException {
        if (this.isOnGround()) {
            System.out.println("Performing maintenance");
            this.setDistanceSinceLastRepair(0);
        } else {
            System.out.println("Cannot perform maintenance in mid air");
            throw new CannotPerformInMidAirException();
        }
    }

    public void loadModule(Module moduleToAdd) throws ModuleNotCompatibleException, NoModuleStationAvailableException {
        if (this.compatibleModuleList.contains(moduleToAdd.getClass())) {
            if (this.getModuleList().size() < this.getModuleStationsAmount()) {
                this.moduleList.add(moduleToAdd);
            } else {
                throw new NoModuleStationAvailableException();
            }
        } else {
            throw new ModuleNotCompatibleException();
        }
    }

    public void activateModule(Class moduleClass, Coordinates destination) throws NoModuleCanPerformException, ModuleNotFoundException {
        boolean hasActivated = false;

        for (Module module : this.getModuleList()) {
            if (module.getClass().equals(moduleClass)) {
                if (module.isDeployable() && isActivationDistanceValid(module.getActivationRange(), destination)) {
                    module.activate(destination);
                    hasActivated = true;
                } else {
                    throw new NoModuleCanPerformException();
                }
            }
        }

        if (!hasActivated) {
            throw new ModuleNotFoundException();
        }
    }

    private boolean isActivationDistanceValid(int range, Coordinates destination) {
        return range >= this.getCurrentLocation().distance(destination);
    }

    public boolean isOnGround() {
        return this.onGround;
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

    public boolean isReadyForFlight() {
        return this.readyForFlight;
    }

    public Coordinates getCurrentLocation() {
        return this.currentLocation;
    }

    private void setCurrentLocation(Coordinates newLocation) {
        this.currentLocation = newLocation;
    }

    public int getMaintenanceDistance() {
        return this.maintenanceDistance;
    }

    public void setMaintenanceDistance(int maintenanceDistance) {
        this.maintenanceDistance = maintenanceDistance;
    }

    protected void setDistanceSinceLastRepair(double newDistance) {
        this.distanceSinceLastRepair = newDistance;
    }

    protected double getDistanceSinceLastRepair() {
        return this.distanceSinceLastRepair;
    }


    public void addCompatibleModule(Class moduleClassToAdd) {
        this.compatibleModuleList.add(moduleClassToAdd);
    }

    public int getModuleStationsAmount() {
        return this.moduleStationsAmount;
    }

    public void setModuleStationsAmount(int moduleStationsAmount) {
        this.moduleStationsAmount = moduleStationsAmount;
    }

    private List<Module> getModuleList() {
        return this.moduleList;
    }
}
